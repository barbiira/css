module.exports = {
    devtool: 'source-map',
    entry: {
        main: './src/app.js',
        drive: './src/drive.js',
        account: './src/account.js',
        lazio: './src/lazio.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}